import $ from 'jquery'

export const helper = {
  isInViewport: ($elem) => {
    if ($(window).width() - ($elem.offset().left + $elem.width()) >= 0) return true
  }
}
