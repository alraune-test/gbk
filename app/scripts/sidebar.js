import $ from 'jquery'

$(function () {
  const $minimizedClass = 'js_sidebar--minimized'
  const $scrollEl = $('.content .simplebar-horizontal')

  $('.sidebar_trigger').on('click', function () {
    $('html').toggleClass($minimizedClass)
    if (!$('body').hasClass($minimizedClass)) $scrollEl.css('display', 'none')
    else $scrollEl.css('visibility', 'visible')
  })

  setMinimized()

  $(window).resize(function () {
    setMinimized()
  })

  function setMinimized () {
    if ($(window).width() <= 768) $('html').addClass($minimizedClass)
    else $('html').removeClass($minimizedClass)
  }
})
