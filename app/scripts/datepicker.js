import $ from 'jquery'
import 'bootstrap-daterangepicker'

$(function () {
  $('.select').daterangepicker({
    opens: 'left',
    autoApply: true,
    parentEl: '.dropdown_body'
  }, function (start, end, label) {
    console.log(start.format('MM/DD/YYYY'), end.format('MM/DD/YYYY'))
  })
})
