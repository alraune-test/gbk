import $ from 'jquery'
import { helper } from './helpers'

$(function () {
  const $select = $('.select')

  $(document).mouseup(e => {
    if (!$select.is(e.target) && $select.has(e.target).length === 0) {
      $select.removeClass('active')
    }
  })

  $('.select_placeholder').on('click', function () {
    const $currentSelect = $(this).closest('.select')

    $currentSelect.toggleClass('active')
    $currentSelect.find('.dropdown').css({ position: 'absolute', visibility: 'hidden', display: 'block' })
    if (!helper.isInViewport($('#datepicker'))) {
      $currentSelect.find('.dropdown').addClass('dropdown--down').width($(this).closest('.select').width())
      $currentSelect.find('.daterangepicker').addClass('daterangepicker--min')
    }
    $currentSelect.find('.dropdown').css({ position: '', visibility: '', display: '' })
  })
})
