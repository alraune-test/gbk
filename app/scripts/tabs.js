import $ from 'jquery'

$(function () {
  const $tabElem = '.tabs_link'
  const $tabClassActive = 'tabs_link--active'
  const $tabContentElem = '.tabs_content-box'
  const $tabContentClassActive = 'tabs_content-box--active'

  $($tabElem).on('click', function (e) {
    e.preventDefault()

    $(this).siblings($tabElem).removeClass($tabClassActive)
    $(this).closest('.tabs').find($tabContentElem).removeClass($tabContentClassActive)
    $(this).addClass($tabClassActive)
    findActiveTab()
  })

  findActiveTab()

  function findActiveTab () {
    $($tabElem).each(function () {
      if ($(this).hasClass($tabClassActive)) {
        $($(this).attr('href')).addClass($tabContentClassActive)
      }
    })
  }
})
