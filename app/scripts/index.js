import '../styles/main.scss'
import './helpers'
import './custom-scroll'
import './sidebar'
import './datepicker'
import './select'
import './tabs'

if (process.env.NODE_ENV !== 'production') {
  require('../index.pug')
}
