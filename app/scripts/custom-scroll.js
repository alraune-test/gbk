import $ from 'jquery'
import SimpleBar from 'simplebar'

$(function () {
  $('.custom-scroll').each(function () {
    // eslint-disable-next-line
        const scroll = new SimpleBar($(this)[0], {autoHide: false})
  })
})
